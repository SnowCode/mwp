<?php
include "Parsedown.php";
include "Git.php";
include "GitRepo.php";
use Kbjr\Git\Git;

$repo = Git::open('content');
$username = $_SERVER['PHP_AUTH_USER'];

function build_file ($name) {
    if (strpos($name, '..') === false) {
        $Parsedown = new Parsedown();
        $md_content = file_get_contents("content/$name.md");
        $content = $Parsedown->text($md_content);

        ob_start();
        include "page.php";
        $result = ob_get_clean();

        $handle = fopen(getcwd() . "/public/$name.html", "w");
        fwrite($handle, $result);
    }
}

if (array_key_exists('edit', $_GET)) {
    $name = $_GET['edit'];
    if (strpos($name, '..') === false) {
        if (realpath("content/$name.md")) {
            $content = file_get_contents("content/$name.md");
        } else {
            $content = "";
        }

        include "editor.php";
    }
}

else if (array_key_exists('submit', $_GET)) {
    $name = $_GET['submit'];
    $content = $_POST['content'];
    if (array_key_exists('summary', $_POST)) {
        $summary = $_POST['summary'];
    } else {
        $summary = "No summary provided";
    }
    if (strpos($name, '..') === false) {
        $handle = fopen("content/$name.md", "w");
        fwrite($handle, $content);
        $repo->add("$name.md");
        $repo->commit($summary);

        build_file($name);
        echo "<meta http-equiv='refresh' content=\"0;url='public/$name.html'\" />";
    }
}
?>
