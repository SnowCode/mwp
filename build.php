<?php
include "wiki.php";

$files = scandir('content/');
foreach ($files as $file) {
    if ($file !== '.git' && $file !== '.' && $file !== '..') {
        $file = str_replace('.md', '', $file);
        build_file($file);
        echo "$file built<br>";
    }
}
?>
